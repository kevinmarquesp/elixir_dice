defmodule ElixirApp do
  @moduledoc """
  Documentation for `ElixirApp`.
  """


  import Plug.Conn


  def init(options) do
    options
  end


  def genRandomNumber() do
    :rand.uniform(6)
  end


  @doc """
  Route that return a json content
  """

  @spec call(Plug.Conn.t(), any) :: Plug.Conn.t()
  def call(conn, _opts) do
    body = Jason.encode!(%{"result" => genRandomNumber()})

    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, body)
  end
end